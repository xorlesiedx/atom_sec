#include <cstddef>
#include <cstdio>
#include <functional>
#include <unordered_set>

/* Using std::pair<int, int> instead of "explicit" 'struct point' reduces code
 * readability, I believe. */
struct point
{
  constexpr point(int _x, int _y)
    : x(_x), y(_y)
  {
  }

  int x;
  int y;
};

/* Inject some types into the 'std' namespace. */
template<>
struct std::hash<struct point>
{
  std::size_t
  operator()(const struct point &val) const noexcept
  {
    std::hash<long long int> hasher;

    return hasher(((long long int)val.y) << 32 | val.x);
  }
};

template<>
struct std::equal_to<struct point>
{
  constexpr bool operator()(const struct point &l, const struct point &r) const
  {
    return l.x == r.x && l.y == r.y;
  }
};

/* Initial data. */
static constexpr struct point k_init_pos(1'000, 1'000);
static constexpr int k_digit_sum_max = 25;

static constexpr int digit_sum(int nmbr);
static void find_valid_dest(const std::unordered_set<struct point> *src,
    std::unordered_set<struct point> *dest);

int constexpr
digit_sum(int nmbr)
{
  int result = 0;
  if (0 > nmbr)
  {
    nmbr *= -1;
  }
  do
  {
    result += nmbr % 10;
    nmbr /= 10;
  }
  while (0 != nmbr);
  return result;
}

void
find_valid_dest(const std::unordered_set<struct point> *src,
    std::unordered_set<struct point> *dest)
{
  std::unordered_set<struct point>::const_iterator it;

  dest->clear();
  for (it = src->cbegin(); src->cend() != it; ++it)
  {
    if (k_digit_sum_max >= digit_sum(it->x) + digit_sum(it->y + 1))
    {
      (void)dest->emplace(it->x, it->y + 1);
    }
    if (k_digit_sum_max >= digit_sum(it->x - 1) + digit_sum(it->y))
    {
      (void)dest->emplace(it->x - 1, it->y);
    }
    if (k_digit_sum_max >= digit_sum(it->x) + digit_sum(it->y - 1))
    {
      (void)dest->emplace(it->x, it->y - 1);
    }
    if (k_digit_sum_max >= digit_sum(it->x + 1) + digit_sum(it->y))
    {
      (void)dest->emplace(it->x + 1, it->y);
    }
  }
}

int
main(void)
{
  std::pair<std::unordered_set<struct point>::iterator, bool> ins;
  std::unordered_set<struct point> dest, src, valid;
  std::unordered_set<struct point>::const_iterator it;

  (void)valid.emplace(k_init_pos.x, k_init_pos.y);
  (void)src.emplace(k_init_pos.x, k_init_pos.y);
  while (0 != src.size())
  {
    find_valid_dest(&src, &dest);
    src.clear();
    for (it = dest.cbegin(); dest.cend() != it; ++it)
    {
      ins = valid.emplace(it->x, it->y);
      if (ins.second)
      {
        src.emplace(it->x, it->y);
      }
    }
  }
  std::printf("Step count: %zu.\n", valid.size());
  return 0;
}
